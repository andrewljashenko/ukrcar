package interfaces;

public interface ProductControllerInterface {
	public void setPrice(int product_id, double price);
	public void setCountStock(int product_id, int countStock);
	public void setTitle(int product_id, String title);
}
