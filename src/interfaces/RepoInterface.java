package interfaces;

import java.util.ArrayList;

import models.Order;



public interface RepoInterface{
	public Object Load ( int id );

	public ArrayList<Object> LoadAll ();

	public void Add ( Object t );

	public void Commit ();
} 
