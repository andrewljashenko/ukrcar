package interfaces;

import java.util.Date;

public interface CarTestDriveControllerInterface {
	public void createBooking(int customer_id, int product_id, Date date);
	public void canceBooking(int booking_id);
}
