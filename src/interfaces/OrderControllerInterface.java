package interfaces;

import java.util.ArrayList;

import models.Customer;
import models.Order;
import models.Product;

public interface OrderControllerInterface {
	
	/**
	 * Create a new Order
	 * 
	 * @param products
	 * @param customer
	 * @param status
	 */
	public void createOrder(int product_id, int customer_id);
	
	/**
	 * Change order status
	 * 
	 * @param order
	 */
	public void setOrderStatus(String status);
	
	/**
	 * Delete order From EntityContainer
	 * 
	 * @param order
	 */
	public void deleteOrder(int order_id);
	
	/**
	 * Change customer in the current Order
	 * 
	 * @param customer
	 */
	public void setOrderCustomer(Customer customer);
	
	/**
	 * set Order existing customer
	 * 
	 * @param customer_id
	 */
	public void setOrderCustomer(int customer_id);
	/**
	 * Change products in the current Order
	 * 
	 * @param products
	 */
	public void setOrderProduct(int order_id, int product_id);
}
