package interfaces;

interface EmailAgentInterface 
{
	void sendEmail ( String targetAddress, String subject, String body );
}
