package interfaces;

import models.Order;

interface OrderRepoInterface extends RepoInterface
{
	public Order findByStatus(String status);
}

 


