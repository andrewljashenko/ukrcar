/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukrcar;

import java.util.Scanner;
import controllers.*;

import models.*;

/**
 *
 * @author Artem
 */
public class App {
    
    private static App instance = null;
    private static Scanner _scanner = new Scanner(System.in);
    
    public static void init(){
        if(App.instance == null){
            App.getInstance();
            App.showInterface();
        }
    }
    
    private static App getInstance(){
        if(App.instance == null)
            App.instance = new App();
        
        return App.instance;
    }
    
    public static void showInterface(){
    	
    	App.loadDefaultData();
    	
    	System.out.println("Выберите действие:\n");
		System.out.println("1. Управление Автосалонами");
		System.out.println("2. Управление Заказами");
	 	System.out.println("3. Управление Обратными звонками");		
	 	System.out.println("4. Управление Пользователями");
	 	
	 	String in = App._scanner.nextLine();
	 	
	 	int choise = 0;
	 	
	 	try{
	 		choise = Integer.parseInt(in);
	 	}
	 	catch(NumberFormatException e){
	 		throw new NumberFormatException("Incorrect Int Value");
	 	}
	 	
	 	if(choise < 1 || choise > 5){
	 		throw new RuntimeException("Incorrent Int Value");
	 	}
	 	
	 	switch(choise){
	 	case 1:
	 		new AutoShowController().actionIndex();
	 		break;
	 	case 2:
	 		new OrderController().actionIndex();
	 		break;
	 	case 3:
	 		new CallbackController().actionIndex();
	 		break;
	 	case 4:
	 		new UserController().actionIndex();
	 		break;
 		default:
 			App.showInterface();
 			break;
	 		
	 	}
	 	
    }
    
    private static void loadDefaultData(){
    	new Callback("Андрей", "+380982467451", "Позвони мне, позвони");
    	new Callback("Александр", "+380982467451", "Позвони мне Ради Бога");
    	new Callback("Маргарита", "+380982467451", "Чтобы заказать такси");
    	new Callback("Таксист", "+380982467451", "Подвезем тебя мы к дому");
    }
    
}
