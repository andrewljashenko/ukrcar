/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Artem
 */
public class Car extends Product{
    
    private String _model;
    private String _brand;
    private CarEngine _engine;
    
    /**
     * Construct
     * @param model
     * @param brand
     * @param price
     * @param countStock
     * @param fuelType
     * @param engineType
     * @param volume 
     */
    
    public Car(String model, String brand, int price, int countStock, String fuelType, String engineType, float volume){
        super(brand + " " + model, price, countStock);
        this.setModel(model);
        this.setBrand(brand);
        this.setEngine(new CarEngine(volume, fuelType, engineType));
        this.setCountStock(countStock);
    }
    
    /**
     * Construct 2
     * @param model
     * @param brand
     * @param price
     * @param countStock
     * @param engine 
     */
    
    public Car(String model, String brand, int price, int countStock, CarEngine engine){
        super(brand + " " + model, price, countStock);
        
        this.setModel(model);
        this.setBrand(brand);
        this.setEngine(engine);
        this.setCountStock(countStock);
    }

    /**
     * Set model of car
     * @param model
     */
    
    public void setModel(String model){
    	if(model.length() < 2){
    		throw new RuntimeException("");
    	}
    	else{
    		this._model = model;
    	}
    }
    
    /**
     * Set car brand
     * @param brand
     */
    
    public void setBrand(String brand){
    	if(brand.length() < 2){
    		throw new RuntimeException("");
    	}
    	else{
    		this._brand = brand;
    	}
    }
    
    /**
     * Set Car Engine
     * @param engine
     */
    
    public void setEngine(CarEngine engine){
    	if(engine == null){
    		throw new RuntimeException("Incorrect CarEngine Object!");
    	}
    	else{
    		this._engine = engine;
    	}
    }
    
    /**
     * set count stock of car
     */
    
    public void setCountStock(int countStock){
    	if(countStock < 0){
    		throw new RuntimeException("Incorrect In Stock count Value");
    	}
    	else{
    		this._countStock = countStock;
    	}
    }
    
    /**
     * get engine object
     * @return 
     */
    
    public CarEngine getEngine(){
        return this._engine;
    }
    
    /**
     * return car model
     * @return 
     */
    
    public String getModel(){
        return this._model;
    }
    
    /**
     * return car brand
     * @return 
     */
    
    public String getBrand(){
        return this._brand;  
    }
    
    /**
     * return Sting info
     * @return 
     */
    
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("ID: " + this.getID() + "\n");
        result.append("Product: " + this.getTitle() + "\n");
        result.append("Cost: $" + this.getPrice() + "\n");
        result.append("Engine: " + this.getEngine().getVolume() + " " + this.getEngine().getEngineType() + " " + this.getEngine().getFuelType() + "\n");
        
        return result.toString();
    }
    
}
