/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.Date;

/**
 *
 * @author Artem
 */
public class CarTestDrive extends Entity{
    
    private Date _date;
    private Car _car;
    private Customer _customer;
    private AutoShow _autoShow;
    
    /**
     * Construct
     * @param autoShow
     * @param date
     * @param car
     * @param customer 
     */
    
    public CarTestDrive(AutoShow autoShow, Date date, Car car, Customer customer){
        super("cartestdrive");
        
        this.setDate(date);
        this.setCar(car);
        this.setCustomer(customer);
        this.setAutoshow(autoShow);
    }
    
    /**
     * set car test drive date
     * @param d
     */
    
    public void setDate(Date d){
    	this._date = d;
    }
    
    /**
     * set autoshow of test drive
     * @param a
     */
    
    public void setAutoshow(AutoShow a){
    	if(a != null){
    		this._autoShow = a;
    	}
    	else throw new RuntimeException("AutoShow object is empty!");
    }
    
    /**
     * set car of test drive
     * @param car
     */
    
    public void setCar(Car car){
    	if(car != null){
    		this._car = car;
    	}
    	else throw new RuntimeException("Car object is empty!");
    }
    
    /**
     * set customer of car test drive
     * @param c
     */
    
    public void setCustomer(Customer c){
    	if(c != null){
    		this._customer = c;
    	}
    	else throw new RuntimeException("Customer Object is empty");
    }
    
    public Car getCar(){
        return this._car;
    }
    
    public Date getStartDate(){
        return this._date;
    }
    
    public AutoShow getAutoShow(){
        return this._autoShow;
    }
    
    public Customer getCustomer(){
    	return this._customer;
    }
    
    /**
     * return info about car test drive
     * @return 
     */
    
    public String toString(){
        
        StringBuilder result = new StringBuilder();
        
        result.append("============== Car Test Drive №: " + this.getID() + " ==============\n");
        result.append("Customer: " + this._customer.getFirstName() + " " + this._customer.getLastName() + "\n");
        result.append("Customer Telephone: " + this._customer.getTelephone() + "\n");
        result.append("Test Drive Date: " + this._date.toString());
        result.append(this._car.toString());
        
        result.append("============== Car Test Drive №: " + this.getID() + " END ==============\n\n");
        
        return result.toString();
        
    }
    
}
