/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Artem
 */
public class AutoShow extends Entity{
    
    private String _address;
    private ArrayList<Car> _products = new ArrayList<Car>();
    
    /**
     * coustruct
     * @param title
     * @param address
     * @param products 
     */
    
    public AutoShow(String title, String address, ArrayList<Car> products){
        super("autoshow");
        
        this.setProducts(products);
        super.setTitle(title);
        this.setAddress(address);
        
    }
    
    public AutoShow(String title, String address){
    	super("autoshow");
    	
    	super.setTitle(title);
        this.setAddress(address);
    }
    
    public ArrayList<Car> getCars(){
    	return _products;
    }
    
    /**
     * Add products to autoshow
     * @param pList
     */
    
    public void setProducts(ArrayList<Car> pList)
    {
    	if(pList == null || pList.isEmpty()){
    		throw new RuntimeException("Your product list is empty!");
    	}
    	else{
    		this._products = pList;
    	}
    }
    
    /**
     * set AutoShow address
     * @param address 
     */
    
    private void setAddress(String address){
        if( address.trim().length() < 5 ){
            throw new RuntimeException("AutoShow.AutoShow: Your must enter correct address!");
        }
        else{
            this._address = address;
        }
    }
    
    /**
     * add car to AutoShow
     * @param car 
     */
    
    public void addCar(Car car){
    	if(car != null){
    		this._products.add(car);
    	}
    	else throw new RuntimeException("Incorrect Car Object");
        
    }
   
    
    /**
     * return Autoshow address
     * @return 
     */
    
    public String getAddress(){
        return this._address;
    }
    
    /**
     * Return object info
     * @return 
     */
    
    public String toString(){
        StringBuilder result = new StringBuilder();
        
        result.append("============== AutoShow №: " + this.getID() + " ==============\n");
        result.append("Title: " + this.getTitle() + "\n");
        result.append("Address: " + this.getAddress() + "\n\n");
        result.append("============== AutoShow Products: " + this._products.size() + " ==============\n");
        for(Product product : this._products){
            result.append(product.toString());
            result.append("--------------------------------------------\n");
        }
        
        result.append("============== AutoShow №: " + this.getID() + " END ==============\n\n");
        
        return result.toString();
    }
    
}
