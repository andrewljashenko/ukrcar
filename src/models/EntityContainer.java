/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Artem
 */
public class EntityContainer {
    
    private static ArrayList<Entity> entityContainer = new ArrayList<Entity>();
    
    /**
     * add Entity Object to container array
     * @param e
     * @return 
     */
    
    public static Entity add(Entity e){
        EntityContainer.entityContainer.add(e);
        return e;
    }
    
    
    /**
     * Delete Entity From Entity List
     * @param entityType
     * @param id
     * @return
     */
    
    public static boolean delete(String entityType, int id){
    	if(entityContainer.isEmpty()) return true;
        
        for(Iterator<Entity> e = EntityContainer.entityContainer.iterator(); e.hasNext(); ){
            
            Entity tempEntity = e.next();
            
            if(tempEntity.getID() == id && tempEntity.getEntityType() == entityType) 
            	e.remove();
            
        }
        
        return true;
    }
    
    /**
     * return Entity Object from static container array
     * @param entityType
     * @param id
     * @return 
     */
    
    public static Entity get(String entityType, int id){
        if(entityContainer.isEmpty()) return null;
        
        for(Entity e : EntityContainer.entityContainer){
            if(e.getID() == id && e.getEntityType() == entityType) return e;
        }
        
        return null;
    }
    
    
    /**
     * 
     * @param entityType
     * @return
     */
    
    public static ArrayList<Entity> get(String entityType){
    	if(entityContainer.isEmpty()) return null;
    	
    	ArrayList<Entity> temp = new ArrayList<Entity>();
    	
    	for(Entity e : EntityContainer.entityContainer){
            if(e.getEntityType() == entityType) 
            	temp.add(e);
        }
        
        return temp;
    }
    
    public static int getLastID(){
    	return EntityContainer.entityContainer.size();
    }
    
}
