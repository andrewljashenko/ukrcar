/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 * Callback Class 
 * @author Artem
 */
public class Callback extends Entity{

    private String _name;
    private String _telephone;
    private String _description;
    
    /**
     * Callback Construct
     * @param name
     * @param telephone
     * @param description 
     */
    
    public Callback(String name, String telephone, String description) {
        super("callback");
        
        this.setName( name );
        this.setTelephone( telephone );
        
        this._description = description;
        
    }
    
    
    /**
     * 
     * @param user
     * @param telephone
     * @param description 
     */
    
    public Callback(Customer user, String description){
        super("callback");
        
        this.setName( user.getFirstName() + " " + user.getLastName() );       
        this.setTelephone( user.getTelephone() );
    }
    
    
    /**
     * 
     * @param name 
     */
    
    private void setName(String name){
        
        if(name.length() == 0){
            throw new RuntimeException("Callback.Callback: Customer name is empty!");
        }
        else{
             this._name = name;
        }
        
    }
    
    /**
     * setter for telephone
     * @param telephone 
     */
    
    private void setTelephone(String telephone){
        
        if(telephone.length() == 0){
            throw new RuntimeException("Callback.Callback: Customer telephone is empty!");
        }
        else{
            this._telephone = telephone;
        }
        
    }
    
    /**
     * Get Customer name
     * @return String
     */
    
    public String getName(){
        return this._name;
    }

    /**
     * Get Customer Telephone
     * @return String
     */
    
    public String getTelephone(){
        return this._telephone;
    }
    
    /**
     * Get Callback Description
     * @return String
     */
    
    public String getDescription(){
        return this._description;
    }
    
    @Override
    public String toString(){
        
        StringBuilder result = new StringBuilder();
        
        result.append("============== Callback №: " + this.getID() + " ==============\n");
        result.append("Customer: " + this.getName() + "\n");
        result.append("Customer Telephone: " + this.getTelephone() + "\n");
        result.append("Description: " + this.getDescription() + "\n");
        
        result.append("============== Callback №: " + this.getID() + " END ==============\n");
        
        return result.toString();
        
    }
    
}
