/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 * Car Engine Class
 * @author Artem
 */
public class CarEngine {
    
    private enum FuelType{
        DIESEL, GASOLINE
    };
    
    private enum EngineType{
    	INLINE_2, INLINE_3, INLINE_4, INLINE_6, V6, V8, V10, V12, VR6
    }
    
    private float _volume;
    private EngineType _engineType;
    private FuelType _fuelType;
    
    /**
     * Construct
     * @param volume
     * @param fuelType
     * @param engineType 
     */
    
    public CarEngine(float volume, String fuelType, String engineType){
        this.setEngineType(engineType);
        this.setFuelType(fuelType);
        this.setVolume( volume );
    }
    
    /**
     * set fueld type
     * @param fuelType
     */
    
    public void setFuelType(String fuelType){
    	this._fuelType = FuelType.valueOf(fuelType);
    }
    
    /**
     * set engine type
     * @param engineType
     */
    
    private void setEngineType(String engineType) {
		this._engineType = EngineType.valueOf(engineType);
	}


	/**
     * set volume of engine
     * @param volume 
     */
    
    private void setVolume(float volume){
        
        if( volume <= 0.2 || volume > 30 ){
            throw new RuntimeException("CarEngine.setVolume: Incorrect Engine volume Value");
        }
        else{
            this._volume = volume;
        }
        
    }
    
    /**
     * Get Volume of Car Engine
     * @return 
     */
    
    public float getVolume(){
        return this._volume;
    }
    
    /**
     * get Fuel Type of engine
     * @return 
     */
    
    public String getFuelType(){
        return this._fuelType.toString();
    }
    
    /**
     * get engine type of car
     * @return 
     */
    
    public String getEngineType(){
        return this._engineType.toString();
    }
    
}
