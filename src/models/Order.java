/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 * Single Order Entity Class
 * 
 * @author Artem
 */

public class Order extends Entity{
    
    private ArrayList<Product> _products = new ArrayList<Product>();
    private Customer _customer;
    private static final String DEFAULT_STATUS = "NEW";
    
    private enum StatusList {
        FAILED, NEW, DRAFT, WAITING_FOR_PAYMNT
    }
    
    private StatusList _status;
    
    /**
     * Construct
     * @param id
     * @param products
     * @param customer 
     */
    
    public Order(ArrayList<Product> products, Customer customer, String status){
        super("order");
        
        this.setProducts(products);
        this.setCustomer(customer);
        this.setStatus(status);
    }
    
    /**
     * Construct
     * @param p
     * @param c
     * @param status
     */
    
    public Order(Product p, Customer c){
    	super("order");
    	
    	this.addProduct(p);
    	this.setCustomer(c);
    	this.setStatus("NEW");
    }
    
    public ArrayList<Product> getProducts(){
    	return this._products;
    }
    
    /**
     * Construct
     * @param c
     */
    
    public Order(Customer c){
    	super("order");
    	
    	this.setStatus(Order.DEFAULT_STATUS);
    	this.setCustomer(c);
    }
    
    /**
     * add product to the order
     * @param p
     */
    
    public void addProduct( Product p ){
    	this._products.add(p);
    }
    
    /**
     * set customer of car test drive
     * @param c
     */
    
    public void setCustomer(Customer c){
    	if(c != null){
    		this._customer = c;
    	}
    	else throw new RuntimeException("Customer Object is empty");
    }
    
    /**
     * set products into order
     * @param products 
     */
    
    private void setProducts( ArrayList<Product> products ){
        if( products.isEmpty() ){
            throw new RuntimeException("Order.setProducts: Your order has no Produts");
        }
        else{
            this._products = products;
        }
    }
    
    /**
     * Get cost of order
     * @return int
     */
    
    public int getOrderCost(){
        int cost = 0;
       
        if(this._products.isEmpty()) return 0;
        
        for(Product product : this._products){
            cost += product.getPrice();
        }
        
        return cost;
    }
    
    /**
     * Return customer of order
     * @return User 
     */
    
    public Customer getCustomer(){
        return this._customer;
    }
    
    /**
     * 
     * @return 
     */
    
    public String getStatus(){
        
        return this._status.toString();
        
    }
    
    
    public void setStatus(String s){
    	this._status = StatusList.valueOf(s);
    }
    
    /**
     * return order info
     * @return String 
     */
    
    @Override
    public String toString(){
        
        StringBuilder result = new StringBuilder();
        
        result.append("============== Order №: " + this.getID() + " ==============\n");
        result.append("Customer: " + this.getCustomer().getFirstName() + " " + this.getCustomer().getLastName() + "\n");
        result.append("Customer Email: " + this.getCustomer().getEmail() + "\n");
        result.append("Customer Telephone: " + this.getCustomer().getTelephone() + "\n");
        result.append("Order Status: " + this.getStatus() + "\n");
        result.append("Order Cost: $" + this.getOrderCost() + "\n\n");
        result.append("============== Order Items: " + this._products.size() + " ==============\n");
        
        for(Product product : this._products){
            result.append(product.toString());
            result.append("--------------------------------------------\n");
        }
        
        result.append("============== Order №: " + this.getID() + " END ==============\n\n");
        
        return result.toString();
        
    }
    
}

