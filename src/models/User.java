/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 * User Entity Class
 * @author Artem
 */

public class User extends Entity{
    
    private int _role;
    private String _email;
    //private String _password;
    private String _firstName;
    private String _lastName;
            
    /**
     * User Construct
     * @param email
     * @param password
     * @param firstName
     * @param lastName 
     */
    
    public User(String email, String password, String firstName, String lastName){
        super("user");
        this.setRole(10);
        this.setEmail(email);
        this.setFirstName(firstName);
        this.setLastName(lastName);
    }
    
    /**
     * set user email
     * @param email
     */
    
    public void setEmail(String email){
    	if(User.isValidEmailAddress(email)){
    		this._email = email;
    	}
    	else throw new RuntimeException("Incorrect User email");
    }
    
    /**
     * set user role
     * @param role
     */
    
    protected void setRole(int role){
    	this._role = role;
    }
    
    /**
     * set last name
     * @param s
     */
    
    public void setLastName(String s){
    	if(s.length() < 2){
    		throw new RuntimeException("Incorrect User Last Name");
    	}
    	else this._lastName = s;
    }
    
    /**
     * set user first name
     * @param s
     */
    
    public void setFirstName(String s){
    	if(s.length() < 2){
    		throw new RuntimeException("Incorrect User First Name");
    	}
    	else this._firstName = s;
    }
    
    /**
     * Return User Email
     * @return String
     */
    
    public String getEmail(){
        return this._email;
    }
    
    /**
     * return User First Name
     * @return String
     */
    
    public String getFirstName(){
        return this._firstName;
    }
    
    /**
     * return User Last Name
     * @return String
     */
    
    public String getLastName(){
        return this._lastName;
    }
    
    /**
     * return user role
     * @return 
     */
    
    public int getRole(){
        return this._role;
    }
    
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("============== User №: " + this.getID() + " ==============\n");
        result.append("User:" + this.getFirstName() + " " + this.getLastName() +"\n");
        result.append("Email:" + this.getEmail() + "\n");
        result.append("============== User №: " + this.getID() + " END ==============\n\n");
        return result.toString();
    }
    
    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
 }
    
}
