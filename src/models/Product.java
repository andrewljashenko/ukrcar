/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 * Product Class
 * @author Artem
 */

public class Product extends Entity{
    
    private double _price;
    protected int _countStock;
    
    /**
     * Construct
     * @param title
     * @param price
     * @param countStock 
     */
    
    public Product(String title, int price, int countStock){
        super("product");
        
        super.setTitle( title );
        this.setCountStock( countStock );
        this.setPrice( price );
    }
    
    
    /**
     * set price of product
     * @param price 
     */
    
    protected void setPrice(double price){
        if( price < 0 ){
            throw new RuntimeException("Product.setCountStock: Incorrect price (must be more than -1)");
        }
        else{
            this._price = price;
        }
    }
    
    /**
     * set count of product in stock
     * @param countStock 
     */
    
    protected void setCountStock(int countStock){
        if( countStock < 0 ){
            throw new RuntimeException("Product.setCountStock: Incorrect count (must be more than -1)");
        }
        else{
            this._countStock = countStock;
        }
    }
    
    /**
     * get cost of product
     * @return 
     */
    
    public double getPrice(){
        return this._price;
    }
    
    /**
     * get count of stock products
     * @return 
     */
    
    public int getInStock(){
        return this._countStock;
    }
    
    /**
     * get boolean value in stock
     * @return 
     */
    
    public boolean isInStock(){
        return this._countStock > 0 ? true : false;
    }
    
    /**
     * return String info of product object
     * @return 
     */
    
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("Product:" + this.getTitle() + "\n");
        result.append("Cost:" + this.getPrice() + "\n");
        
        return result.toString();
    }
    
}
