/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Artem
 */
public class Customer extends User{
    
    private String _telephone;
    
    /**
     * Construct
     * @param user
     * @param password 
     */
    
    public Customer(User user, String password, String telephone){
        super(user.getEmail(), password, user.getFirstName(), user.getLastName());
        this.setTelephone(telephone);
        super.setRole(9);
    }
    
    /**
     * Construct
     * @param email
     * @param password
     * @param firstName
     * @param lastName 
     */
    
    public Customer(String email, String password, String firstName, String lastName, String telephone){
        super(email, password, firstName, lastName);
        this.setTelephone(telephone);
        super.setRole(9);
    }
    
    /**
     * set customer telephone
     * @param telephone
     */
    
    public void setTelephone(String telephone){
    	if(telephone.length() < 5){
    		throw new RuntimeException("Incorrect customer telephone");
    	}
    	else{
    		this._telephone = telephone;
    	}
    	
    }
    
    /**
     * return customer telephone
     * @return 
     */
    
    public String getTelephone(){
        return this._telephone;
    }
    
    @Override
    public String toString(){
        StringBuilder result = new StringBuilder();
        result.append("============== User №: " + this.getID() + " ==============\n");
        result.append("User:" + this.getFirstName() + " " + this.getLastName() +"\n");
        result.append("Email:" + this.getEmail() + "\n");
        result.append("Telephone:" + this.getTelephone() + "\n");
        result.append("============== User №: " + this.getID() + " END ==============\n\n");
        return result.toString();
    }
    
}
