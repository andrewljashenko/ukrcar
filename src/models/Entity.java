/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author Artem
 */
public abstract class Entity {
    
    private String _title;
    private String _entityType;
    private int _id;
    
    /**
     * Construct
     * @param entityType 
     */
    
    public Entity(String entityType){
    	this._id = EntityContainer.getLastID() + 1;
        this._entityType = entityType;
        EntityContainer.add(this);
    }
    
    
    /**
     * return title of the entity
     * @return 
     */
    
    public String getTitle(){
        return this._title;
    }
    
    /**
     * set Entity title
     * @param title 
     */
    
    protected void setTitle(String title){
        
        if( title.trim().length() == 0 ){
            throw new RuntimeException("Your Entity has no any title!");
        }
        else{
            this._title = title;
        }
        
    }
    
    /**
     * return Entity ID
     * @return 
     */
    
    public int getID(){
        return this._id;
    }
    
    /**
     * get entity type
     * @return 
     */
    
    public String getEntityType(){
        return this._entityType;
    }
    
    /**
     * get entity object
     * @param entityType
     * @param id
     * @return 
     */
    
    public static Entity load(String entityType, int id){
        
        return EntityContainer.get(entityType, id);
        
    }
    
    
    public static void delete(String entityType, int id){
    	
    	EntityContainer.delete(entityType, id);
    	
    }
    
    /**
     * get entities arraylist
     * @param entityType
     * @return
     */
    
    public static ArrayList<Entity> load(String entityType){
    	
    	return EntityContainer.get(entityType);
    	
    }
    
    abstract public String toString();
    
}
