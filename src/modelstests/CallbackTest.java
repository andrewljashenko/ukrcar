package modelstests;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Callback;
import models.Customer;

class CallbackTest {

	Callback callbackNotCustomer = new Callback("Andrei Liashenko", "+380982467451", "Test Description");
	
	Customer c = new Customer("andrewljashenko@gmail.com", "", "Andrei","Liashenko", "+380982467451");
	Callback callbackCustomer = new Callback(c, "Test Description");
	
	@Test
	public void checkCustomerName(){
		assertNotNull( callbackNotCustomer.getName() );
		assertNotNull( callbackCustomer.getName() );
		assertTrue( callbackNotCustomer.getName().length() > 2 );
		assertTrue( callbackCustomer.getName().length() > 2 );
	}
	
	@Test
	public void callbackHasTelephone(){
		assertNotNull( callbackNotCustomer.getTelephone() );
		assertNotNull( callbackCustomer.getTelephone() );
	}
	
	@Test
	public void callbackHasValidTelephone(){
		assertTrue( callbackNotCustomer.getTelephone().length() > 5 );
		assertTrue( callbackCustomer.getTelephone().length() > 5 );
	}
	
	@Test
	public void callbackHasDescription(){
		assertNotNull( callbackNotCustomer.getDescription() );
		assertNotNull( callbackCustomer.getDescription() );
		assertTrue( callbackNotCustomer.getDescription().length() > 5 );
		assertTrue( callbackCustomer.getDescription().length() > 5 );
	}
	
}
