package modelstests;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Customer;
import models.User;

/**
 * 
 * @author pekston
 *
 */

class CustomerTest {

	private Customer testCustomer = new Customer("aliashenko@corp.web4pro.com.ua", "", 
			"Andrei", "Liashenko", "+380982467451");
	
	@Test
	public void checkCustomerInitializedRole(){
		assertEquals( testCustomer.getRole(), 9 );
	}
	
	@Test
	public void checkCustomerTelephoneTest()
	{
		assertNotNull( testCustomer.getTelephone() );
		assertTrue( testCustomer.getTelephone().length() > 4 );
	}
	
	@Test
	public void checkCustomerEmailTest()
	{
		assertTrue( User.isValidEmailAddress(testCustomer.getEmail()) );
	}
	
	@Test
	public void checkCustomerNamesTest()
	{
		assertFalse( testCustomer.getFirstName().isEmpty() );
		assertFalse( testCustomer.getLastName().isEmpty() );
		assertTrue( testCustomer.getFirstName().length() >= 2 );
		assertTrue( testCustomer.getLastName().length() >= 2 );
	}
	
}
