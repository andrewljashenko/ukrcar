package modelstests;

import static org.junit.Assert.*;

import models.User;

import org.junit.Test;

class UserTest {

	private User userTest = new User("andrewljashenko@gmail.com", "", "Andrei", "Liashenko");
	
	@Test
	public void userGetInitializedRoleTest()
	{
		assertEquals( userTest.getRole(), 10 );
	}
	
	@Test
	public void userCheckInitializedUserRoleTest()
	{
		assertNotNull(userTest.getRole());
	}
	
	@Test
	public void userCheckNamesTest()
	{
		assertFalse( userTest.getFirstName().isEmpty() );
		assertFalse( userTest.getLastName().isEmpty() );
		assertTrue( userTest.getFirstName().length() >= 2 );
		assertTrue( userTest.getLastName().length() >= 2 );
	}
	
	@Test
	public void userCheckValidEMailTest()
	{
		assertTrue( User.isValidEmailAddress( userTest.getEmail() ) );
	}
	
}
