package modelstests;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Car;

class CarTest {

	private Car c = new Car("GTR Alpha 12+", "Nissan", 100000, 5, "DIESEL", "v6", (float) 3.6);
	
	@Test
	public void isCarInitialized(){
		assertNotNull( c );
	}
	
	@Test
	public void hasCarEngineTest()
	{
		assertNotNull( c.getEngine() );
	}
	
	@Test
	public void hasCarValidAvailableCountTest()
	{
		assertTrue( c.getInStock() >= 0 );
	}
	
	@Test
	public void hasCarValidPriceTest()
	{
		assertTrue( c.getPrice() >= 0 );
	}
	
	@Test
	public void hasCarValidNamesTest()
	{
		assertTrue( c.getBrand().length() > 2 || c.getModel().length() > 2 );
	}
	
	@Test
	public void isCarEngineValid()
	{
		assertTrue( c.getEngine().getFuelType() == "DIESEL" ||
					c.getEngine().getFuelType() == "GASOLINE" );
		assertTrue( c.getEngine().getVolume() > 0.2 && 
					c.getEngine().getVolume() < 30 );
	}
	
}
