package modelstests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import models.AutoShow;
import models.Car;

class AutoShowTest {

	private AutoShow autoShowTest = new AutoShow("Nissan Motors", "пр. Московский 5", new ArrayList<Car>());
	
	@Test
	public void checkAutoShowCarsTest(){
		assertNull( autoShowTest.getCars() );
		autoShowTest.addCar( new Car("GTR Alpha 12+", "Nissan", 100000, 5, "DIESEL", "v6", (float) 3.6) );
		assertNotNull( autoShowTest.getCars() );
	}
	
	@Test
	public void checkAutoShowAddressTest(){
		assertNotNull( autoShowTest.getAddress() );
		assertTrue( autoShowTest.getAddress().length() > 5 );
	}
	
	@Test
	public void checkAutoShowTitleTest(){
		assertNotNull( autoShowTest.getTitle() );
	}
	
}
