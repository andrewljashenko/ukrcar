package modelstests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;

import models.AutoShow;
import models.Car;
import models.CarTestDrive;
import models.Customer;
import models.User;

import org.junit.Test;

class CarTestDriveTest {

	private AutoShow autoShowTest = new AutoShow("Nissan Motors", "пр. Московский 5", new ArrayList<Car>());
	private CarTestDrive c = new CarTestDrive(
			autoShowTest, 
			(java.sql.Date) new Date(),
			new Car("GTR Alpha 12+", "Nissan", 100000, 5, "DIESEL", "v6", (float) 3.6),
			new Customer( "aliashenko@corp.web4pro.com.ua", "", "Andrei", "Liashenko", "+380982467451" ));
	
	@Test
	public void checkTestDriveCarTest()
	{
		assertNotNull( c.getCar() );
	}
	
	@Test
	public void checkTestDriveAutoShowTest()
	{
		assertNotNull( c.getAutoShow() );
	}
	
	@Test
	public void checkTestDiveCustomerTest()
	{
		assertNotNull( c.getCustomer() );
	}
	
	@Test
	public void checkCustomerEmailTest()
	{
		assertTrue( User.isValidEmailAddress(c.getCustomer().getEmail()) );
	}
	
	@Test
	public void checkCustomerNamesTest()
	{
		assertFalse( c.getCustomer().getFirstName().isEmpty() );
		assertFalse( c.getCustomer().getLastName().isEmpty() );
		assertTrue( c.getCustomer().getFirstName().length() >= 2 );
		assertTrue( c.getCustomer().getLastName().length() >= 2 );
	}
	
	@Test
	public void checkCustomerTelephoneTest()
	{
		assertNotNull( c.getCustomer().getTelephone() );
		assertTrue( c.getCustomer().getTelephone().length() > 4 );
	}
	
	@Test
	public void hasCarValidNamesTest()
	{
		assertTrue( c.getCar().getBrand().length() > 2 || c.getCar().getModel().length() > 2 );
	}
	
	@Test
	public void isCarAvailable()
	{
		assertTrue( c.getCar().isInStock() );
	}
	
}
