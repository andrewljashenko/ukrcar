package modelstests;

import static org.junit.Assert.*;

import org.junit.Test;

import models.Customer;
import models.Order;
import models.Product;

class OrderTest {

	Order oTest = new Order(new Product("Test Product", 100, 4), 
				  new Customer("aliashenko@corp.web4pro.com.ua", "", 
						  "Andrei", "Liashenko", "+380982467451")
		      );
	
	@Test
	public void isOrderStatusInitialized(){
		assertNotNull( oTest.getStatus() );
	}
	
	@Test
	public void checkOrderInitializedStatusTest()
	{
		assertEquals( oTest.getStatus(), "NEW" );
	}
	
	@Test
	public void checkOrderChangedStatusTest()
	{
		oTest.setStatus("FAILED");
		assertEquals( oTest.getStatus(), "FAILED" );
	}
	
	@Test
	public void checkOrderProductsTest()
	{
		assertNotNull( oTest.getProducts() );
	}
	
	@Test
	public void checkOrderCustomerTest(){
		assertNotNull( oTest.getCustomer() );
		assertNotNull( oTest.getCustomer() );
		assertTrue( oTest.getCustomer().getFirstName().length() > 2 &&
					oTest.getCustomer().getLastName().length() > 2 &&
					oTest.getCustomer().getEmail().length() > 2 );
	}
	
	@Test
	public void checkOrderCostTest(){
		assertTrue( oTest.getOrderCost() >=0 );
	}
	
}
