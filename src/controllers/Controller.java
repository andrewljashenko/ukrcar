package controllers;

import java.util.ArrayList;
import java.util.Scanner;

import models.Entity;

import ukrcar.App;

abstract class Controller {

	protected Scanner _scanner = new Scanner(System.in);
	protected int _choise = 0;
	
	
	/**
	 * Default controller Action
	 */
	
	abstract public void actionIndex();
	
	/**
	 * Action Create Entity
	 */
	
	abstract protected void actionCreate();
	
	/**
	 * delete entity action
	 * @param entityType
	 */
	
	protected void actionDelete(String entityType)
	{
		System.out.println("Введите ID " + entityType);
		String temp = this._scanner.nextLine();
		
		//И тут говнокодом попахивает...
		int id = this.getCorrectChoise(temp, 0, 1000000000);
		
		Entity.delete(entityType, id);
		
		this.actionIndex();
	}
	
	/**
	 * SHow entity Info
	 * @param s
	 */
	
	protected void actionShowAll(String s){
		
		ArrayList<Entity> temp = Entity.load(s);
		
		if(temp.isEmpty()){
			System.out.println("You have no any " + s + "s");
		}
		else{
			for(Entity a: temp){
				System.out.println(a);
			}
		}
		
		this.actionIndex();
		
	}
	/**
	 * 
	 * @param data
	 * @param s1
	 * @param s2
	 * @return
	 */
	
	public boolean isCorrectChoise(String data, int s1, int s2){
		
		int choise = 0;
		
		try{
			choise = Integer.parseInt(data);
		}
		catch(NumberFormatException e){
			throw new NumberFormatException("Incorrect Int Value");
		}
		
		if(s1 == -1 && s2 == -1) 
			return true;
		
		if(choise < s1 || choise > s2){
			throw new NumberFormatException("Incorrect Int Value");
		}
		return true;
		
	}
	
	
	/**
	 * 
	 * @param s
	 * @param s1
	 * @param s2
	 * @return
	 */
	
	public int getCorrectChoise(String s, int s1, int s2){
		if(this.isCorrectChoise(s, s1, s2)){
			return Integer.parseInt(s);
		}
		return 0;
	}
	
}
