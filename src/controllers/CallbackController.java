package controllers;

import ukrcar.App;

public class CallbackController extends Controller{

	public void actionIndex(){
		
		System.out.println("Выберите действие:\n");
		System.out.println("1. Показать все");
		System.out.println("2. Удалить");
		System.out.println("3. Вернуться в главное меню");
		
		String c = super._scanner.nextLine();
		
		this._choise = super.getCorrectChoise(c, 1, 3);
		
		switch(this._choise){
			case 1:
				super.actionShowAll("callback");
				break;
			case 2:
				super.actionDelete("callback");
				break;
			case 3:
				App.showInterface();
				break;
			default:
				this.actionIndex();
		}
		
	}
	
	protected void actionCreate(){}
	
}
