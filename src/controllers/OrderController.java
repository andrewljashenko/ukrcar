package controllers;

import ukrcar.App;
import models.Car;
import models.Customer;
import models.Entity;
import models.Order;
import interfaces.OrderControllerInterface;

public class OrderController extends Controller implements OrderControllerInterface{

	/**
	 * Default Controller Action
	 */
	
	public void actionIndex(){
		
		System.out.println("Выберите действие:\n");
		System.out.println("1. Показать все");
		System.out.println("2. Удалить заказ");
		System.out.println("3. Изменить статус заказа");
		System.out.println("4. Создать заказ");
		System.out.println("5. Добавить товар к заказу");
		System.out.println("6. Вернуться в главное меню");
		
		String c = this._scanner.nextLine();
		
		this._choise = super.getCorrectChoise(c, 1, 6);
		
		switch(this._choise){
			case 1:
				super.actionShowAll("order");
				break;
			case 2:
				super.actionDelete("order");
				break;
			case 3:
				this.actionChangeStatus();
				break;
			case 4:
				this.actionCreate();
				break;
			case 5:
				this.actionAddOrderProduct();
				break;
			case 6:
				App.showInterface();
				break;
			default:
				this.actionIndex();
				
		}
		
	}
	
	/**
	 * Create Order
	 */
	
	public void actionCreate()
	{
		System.out.println("Пользователь существует, или создать нового? (new / exists)");
		String c = super._scanner.nextLine();
		
		Customer customer;
		
		if(c.equalsIgnoreCase("new")){
			
			System.out.println("Введите email пользователя:");
			String email = super._scanner.nextLine();
			
			System.out.println("Введите Имя пользователя:");
			String firstName = super._scanner.nextLine();
			
			System.out.println("Введите Фамилию пользователя:");
			String lastName = super._scanner.nextLine();
			
			System.out.println("Телефон пользователя:");
			String telephone = super._scanner.nextLine();
			
			customer = new Customer(email, "", firstName, lastName, telephone);
			
			Order o = new Order(customer);
			System.out.println(o);
			
		}
		else if(c.equalsIgnoreCase("exists")){
			System.out.println("Введите ID пользователя:");
			String sUid = super._scanner.nextLine();
			
			//Точка Высадки капитана Прайса
			customer = (Customer) Entity.load("user", super.getCorrectChoise(sUid, 1, 1000000000));
			
			if(customer != null){
				Order o = new Order(customer);
				System.out.println(o);
			}
			else{
				throw new RuntimeException("This user is not exists");
			}
		}
		else{
			throw new RuntimeException("Your answer is incorrect!");
		}
		
		this.actionIndex();
		
	}
	
	private void actionAddOrderProduct(){
		
		System.out.println("Введите ID заказа:");
		String orderID = super._scanner.nextLine();
		
		//...огромная лаба
		int oID = super.getCorrectChoise(orderID, 1, 1000000000);
		
		System.out.println("Введите ID товара:");
		String productID = super._scanner.nextLine();
		
		//Поставьте отработку пл3
		//Мне лень переписать функцию нормально:)
		int pID = super.getCorrectChoise(productID, 1, 1000000000);
		
		Car c = (Car) Entity.load("product", pID);
		Order o = (Order) Entity.load("order", oID);
		
		if(c != null && o != null){
			o.addProduct(c);
		}
		else{
			throw new RuntimeException("Your Car or Order is not exists");
		}
		
		this.actionIndex();
		
	}
	
	/**
	 * Change status of order by order id
	 */
	
	public void actionChangeStatus(){
		
		System.out.println("Введите ID заказа:");
		String c = this._scanner.nextLine();
		
		//Век живи, век пиши говнокод
		this._choise = super.getCorrectChoise(c, 1, 1000000000);	
		
		Order o = (Order) Entity.load("order", this._choise);
		
		if(o != null){
			o.setStatus(this._scanner.nextLine());
			System.out.println(o);
		}
		else{
			throw new RuntimeException("You have no orders by this ID");
		}
		
		this.actionIndex();
		
	}

	@Override
	public void createOrder(int product_id, int customer_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setOrderStatus(String status) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteOrder(int order_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setOrderCustomer(Customer customer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setOrderCustomer(int customer_id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setOrderProduct(int order_id, int product_id) {
		// TODO Auto-generated method stub
		
	}
	
}
