package controllers;

import ukrcar.App;
import models.Customer;

public class UserController extends Controller{

	public void actionIndex()
	{	
		System.out.println("Выберите действие:\n");
		System.out.println("1. Показать всех пользователей");
		System.out.println("2. Создать пользователя");
		System.out.println("3. Удалить пользователя");
		System.out.println("4. Вернуться в главное меню");
		
		String c = super._scanner.nextLine();
		this._choise = super.getCorrectChoise(c, 1, 4);
		
		switch(this._choise){
			case 1:
				super.actionShowAll("user");
				break;
			case 2:
				this.actionCreate();
				break;
			case 3:
				this.actionDelete("user");
				break;
			case 4:
				App.showInterface();
				break;
			default:
				this.actionIndex();
		}
		
	}
	
	public void actionCreate()
	{
		System.out.println("Введите email пользователя:");
		String email = super._scanner.nextLine();
		
		System.out.println("Введите Имя пользователя:");
		String firstName = super._scanner.nextLine();
		
		System.out.println("Введите Фамилию пользователя:");
		String lastName = super._scanner.nextLine();
		
		System.out.println("Телефон пользователя:");
		String telephone = super._scanner.nextLine();
		
		Customer customer = new Customer(email, "", firstName, lastName, telephone);
		
		System.out.println(customer);
		
		this.actionIndex();
		
	}
	
}
