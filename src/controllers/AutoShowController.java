package controllers;

import ukrcar.App;
import models.AutoShow;
import models.Entity;
import models.Car;

public class AutoShowController extends Controller{
	
	
	/**
	 * Default Controller action
	 */
	
	public void actionIndex(){
		
		this._choise = 0;
		
		System.out.println("Выберите действие: \n");
		System.out.println("1. Создать автосалон");
		System.out.println("2. Вывести все автосалоны");
		System.out.println("3. Добавить автомобиль в салон");
		System.out.println("4. Удалить автосалон");
		System.out.println("5. Вернуться в главное меню");
		
		String in = this._scanner.nextLine();
		
		this._choise = this.getCorrectChoise(in, 1, 5);
		
		switch(this._choise){
		case 1:
			this.actionCreate();
			break;
		case 2:
			this.actionShowAll("autoshow");
			break;
		case 3:
			this.actionAddCarToAutoShow();
			break;
		case 4:
			super.actionDelete("autoshow");
			break;
		case 5:
			App.showInterface();
			break;
		default:
			this.actionIndex();
		}
		
	}


	/**
	 * Create new AutoShow
	 */
	
	public void actionCreate(){
		
		System.out.println("\nВведите название автосалона:");
		String autoshowTitle = this._scanner.nextLine();
		
		System.out.println("Введите адрес автосалона:");
		String autoshowAddress = this._scanner.nextLine();
		
		AutoShow autoshow = new AutoShow(autoshowTitle, autoshowAddress);
		
		System.out.println(autoshow);
		
		this.actionIndex();
		
	}
	
	
	
	/**
	 * Add car to specific autoshow
	 */
	
	public void actionAddCarToAutoShow(){
		
		this._choise = 0;
		
		System.out.println("Enter ID of AutoShow");
		String s = this._scanner.nextLine();
		
		this._choise = this.getCorrectChoise(s, -1, -1);
		
		AutoShow a = (AutoShow) Entity.load("autoshow", this._choise);
		
		if( a != null ){
			System.out.println("Введите модель автомобиля:");
			String carModel = this._scanner.nextLine();
			
			System.out.println("Введите марку автомобиля:");
			String carBrand = this._scanner.nextLine();
			
			System.out.println("Стоимость автомобиля:");
			String s2 = this._scanner.nextLine();
			
			System.out.println("Сколько автомобилей в наличии?:");
			String availability = this._scanner.nextLine();
			
			System.out.println("Введите тип топлива (DIESEL / GASOLINE):");
			String fuelType = this._scanner.nextLine();
			
			System.out.println("Введите объем двигателя:");
			String s5 = this._scanner.nextLine();
			
			System.out.println("Тип двигателя (Inline 4 / v6 / v8 / v10 / v12 / vr6 etc):");
			String engineType = this._scanner.nextLine();
			
			
			//Предельно осторожно, говнокод!
			Car car = new Car(carModel, carBrand, this.getCorrectChoise(s2, -1, -1),
					this.getCorrectChoise(availability, 0, 100000000), fuelType,
					engineType, Float.parseFloat(s5));
			
			a.addCar(car);
			
			System.out.println(a);
			
		}
		else{
			System.out.println("You have no Autoshow by this id!");
		}
		
		this.actionIndex();
		
	}

}
